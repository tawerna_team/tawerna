﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Tawerna_0._1
{
    public partial class Tawerna : MetroFramework.Forms.MetroForm
    {
        static string connStr; // string variable for connection with tawerna database
        MySqlConnection myConnection; //object for connection with tawerna database
        // Granting access to database: GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root'
        int post_counter = 0; //variable used for counting posts

        public Tawerna()
        {
            InitializeComponent();
            connStr = "SERVER = 10.224.184.79; PORT=3306; DATABASE=tawerna_db; UID=root; PASSWORD=root;";
            myConnection = new MySqlConnection(connStr);
            ObtainDataFromDB();
        }

        private void add_Click(object sender, EventArgs e)
        {
            // open new panel for adding new post
            Add_Panel openFom = new Add_Panel();
            openFom.Show();
        }

        public void ObtainDataFromDB()
        {
            // sql query for obtaining post data from database
            string sql = string.Format("select * from tw_posts order by post_id desc limit 3");

            // try-catch block captures bugs
            try
            {
                MySqlCommand cmd = new MySqlCommand(sql, myConnection);

                // open connection with database
                myConnection.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    switch (post_counter)
                    {
                        case 0:
                            Subject_newest_txt.Text = reader.GetString("post_subject");
                            Post_newest_txt.Text = reader.GetString("post_text");
                            Date_newest_txt.Text = reader.GetString("post_cd");
                            break;
                        case 1:
                            Subject_2_txt.Text = reader.GetString("post_subject");
                            Post_2_txt.Text = reader.GetString("post_text");
                            Date_2_txt.Text = reader.GetString("post_cd");
                            break;
                        case 2:
                            Subject_3_txt.Text = reader.GetString("post_subject");
                            Post_3_txt.Text = reader.GetString("post_text");
                            Date_3_txt.Text = reader.GetString("post_cd");
                            break;
                    }

                    post_counter++; // Increase post_counter
                }

                //Close connection after executing query
                myConnection.Close();

            }
            //Catch bug and show proper message
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                MessageBox.Show("Insertion of new data into database has been failed!\n\n" + ex, "Error");
            }

            post_counter = 0; // Zero post_counter
        }

        private void reload_Click(object sender, EventArgs e)
        {
            ObtainDataFromDB();
        }

        private void accounts_Click(object sender, EventArgs e)
        {
            // open new panel for adding new post
            Accounts openFom = new Accounts();
            openFom.Show();
        }
    }
}
