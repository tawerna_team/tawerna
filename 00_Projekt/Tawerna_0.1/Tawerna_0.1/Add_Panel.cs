﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tulpep.NotificationWindow;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Tawerna_0._1
{
    public partial class Add_Panel : MetroFramework.Forms.MetroForm
    {
        static string connStr; // string variable for connection with tawerna database
        MySqlConnection myConnection; //object for connection with tawerna database
        // Granting access to database: GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root'

        public Add_Panel()
        {
            InitializeComponent();
            connStr = "SERVER = 10.224.184.79; PORT=3306; DATABASE=tawerna_db; UID=root; PASSWORD=root;";
            myConnection = new MySqlConnection(connStr);
        }

        private void add_button_Click(object sender, EventArgs e)
        {

            // show popup notifier using PopupNotifier class
            PopupNotifier popup = new PopupNotifier();
            popup.Image = Properties.Resources.notify_image;
            popup.TitleText = "Tawerna Notification";
            popup.ContentText = "Someone has added new post to Tawerna!";
            popup.Popup();

            // calling InsertDataToDB method
            InsertDataToDB();

            // close Add window
            this.Close();
        }

        public void InsertDataToDB()
        {
            // sql query for inserting post data into database
            string sql = string.Format("insert into tw_posts " +
                                     "(post_subject, post_text, post_cd) " +
                                     "values ('{0}', '{1}', '{2}');",
                                     Subject_txt_box.Text,
                                     Post_txt_box.Text,
                                     DateTime.Now.ToString()
                                     );

            // try-catch block captures bugs
            try
            {
                // open connection with database
                myConnection.Open();

                // execute SQL qery on previously opened connection
                using (MySqlCommand cmd = new MySqlCommand(sql, this.myConnection))
                {
                    cmd.ExecuteNonQuery();
                }
                //Close connection after executing query
                myConnection.Close();

            }
            //Catch bug and show proper message
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                MessageBox.Show("Insertion of new data into database has been failed!\n\n" + ex, "Error");
            }

        }
    }
}
