﻿namespace Tawerna_0._1
{
    partial class Add_Panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Panel));
            this.add_button = new MetroFramework.Controls.MetroButton();
            this.Subject_txt_box = new MetroFramework.Controls.MetroTextBox();
            this.Post_txt_box = new MetroFramework.Controls.MetroTextBox();
            this.Subject_label = new MetroFramework.Controls.MetroLabel();
            this.Content_label = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // add_button
            // 
            this.add_button.Location = new System.Drawing.Point(168, 541);
            this.add_button.Name = "add_button";
            this.add_button.Size = new System.Drawing.Size(115, 47);
            this.add_button.Style = MetroFramework.MetroColorStyle.White;
            this.add_button.TabIndex = 0;
            this.add_button.Text = "Add";
            this.add_button.Theme = MetroFramework.MetroThemeStyle.Light;
            this.add_button.Click += new System.EventHandler(this.add_button_Click);
            // 
            // Subject_txt_box
            // 
            this.Subject_txt_box.Location = new System.Drawing.Point(69, 108);
            this.Subject_txt_box.Multiline = true;
            this.Subject_txt_box.Name = "Subject_txt_box";
            this.Subject_txt_box.Size = new System.Drawing.Size(299, 59);
            this.Subject_txt_box.TabIndex = 1;
            // 
            // Post_txt_box
            // 
            this.Post_txt_box.Location = new System.Drawing.Point(69, 230);
            this.Post_txt_box.Multiline = true;
            this.Post_txt_box.Name = "Post_txt_box";
            this.Post_txt_box.Size = new System.Drawing.Size(299, 271);
            this.Post_txt_box.TabIndex = 2;
            // 
            // Subject_label
            // 
            this.Subject_label.AutoSize = true;
            this.Subject_label.Location = new System.Drawing.Point(69, 86);
            this.Subject_label.Name = "Subject_label";
            this.Subject_label.Size = new System.Drawing.Size(51, 19);
            this.Subject_label.TabIndex = 3;
            this.Subject_label.Text = "Subject";
            // 
            // Content_label
            // 
            this.Content_label.AutoSize = true;
            this.Content_label.Location = new System.Drawing.Point(69, 208);
            this.Content_label.Name = "Content_label";
            this.Content_label.Size = new System.Drawing.Size(55, 19);
            this.Content_label.TabIndex = 4;
            this.Content_label.Text = "Content";
            // 
            // Add_Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 611);
            this.Controls.Add(this.Content_label);
            this.Controls.Add(this.Subject_label);
            this.Controls.Add(this.Post_txt_box);
            this.Controls.Add(this.Subject_txt_box);
            this.Controls.Add(this.add_button);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Add_Panel";
            this.Text = "New Post";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton add_button;
        private MetroFramework.Controls.MetroTextBox Subject_txt_box;
        private MetroFramework.Controls.MetroTextBox Post_txt_box;
        private MetroFramework.Controls.MetroLabel Subject_label;
        private MetroFramework.Controls.MetroLabel Content_label;
    }
}