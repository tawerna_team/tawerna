﻿namespace Tawerna_0._1
{
    partial class Tawerna
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Tawerna));
            this.add = new MetroFramework.Controls.MetroTile();
            this.Post_newest_txt = new MetroFramework.Controls.MetroTextBox();
            this.Subject_newest_txt = new MetroFramework.Controls.MetroTextBox();
            this.Subject_label = new MetroFramework.Controls.MetroLabel();
            this.Content_label = new MetroFramework.Controls.MetroLabel();
            this.reload = new MetroFramework.Controls.MetroTile();
            this.Content_label_2 = new MetroFramework.Controls.MetroLabel();
            this.Subject_label_2 = new MetroFramework.Controls.MetroLabel();
            this.Subject_2_txt = new MetroFramework.Controls.MetroTextBox();
            this.Post_2_txt = new MetroFramework.Controls.MetroTextBox();
            this.Content_label_3 = new MetroFramework.Controls.MetroLabel();
            this.Subject_label_3 = new MetroFramework.Controls.MetroLabel();
            this.Subject_3_txt = new MetroFramework.Controls.MetroTextBox();
            this.Post_3_txt = new MetroFramework.Controls.MetroTextBox();
            this.Date_newest_txt = new MetroFramework.Controls.MetroTextBox();
            this.Date_2_txt = new MetroFramework.Controls.MetroTextBox();
            this.Date_3_txt = new MetroFramework.Controls.MetroTextBox();
            this.accounts = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // add
            // 
            this.add.ForeColor = System.Drawing.SystemColors.ControlText;
            this.add.Location = new System.Drawing.Point(826, 95);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(56, 63);
            this.add.TabIndex = 0;
            this.add.TileImage = ((System.Drawing.Image)(resources.GetObject("add.TileImage")));
            this.add.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.add.UseTileImage = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // Post_newest_txt
            // 
            this.Post_newest_txt.Location = new System.Drawing.Point(107, 214);
            this.Post_newest_txt.Multiline = true;
            this.Post_newest_txt.Name = "Post_newest_txt";
            this.Post_newest_txt.ReadOnly = true;
            this.Post_newest_txt.Size = new System.Drawing.Size(245, 159);
            this.Post_newest_txt.TabIndex = 1;
            // 
            // Subject_newest_txt
            // 
            this.Subject_newest_txt.Location = new System.Drawing.Point(107, 147);
            this.Subject_newest_txt.Multiline = true;
            this.Subject_newest_txt.Name = "Subject_newest_txt";
            this.Subject_newest_txt.ReadOnly = true;
            this.Subject_newest_txt.Size = new System.Drawing.Size(245, 27);
            this.Subject_newest_txt.TabIndex = 2;
            // 
            // Subject_label
            // 
            this.Subject_label.AutoSize = true;
            this.Subject_label.Location = new System.Drawing.Point(107, 125);
            this.Subject_label.Name = "Subject_label";
            this.Subject_label.Size = new System.Drawing.Size(51, 19);
            this.Subject_label.TabIndex = 4;
            this.Subject_label.Text = "Subject";
            // 
            // Content_label
            // 
            this.Content_label.AutoSize = true;
            this.Content_label.Location = new System.Drawing.Point(107, 192);
            this.Content_label.Name = "Content_label";
            this.Content_label.Size = new System.Drawing.Size(55, 19);
            this.Content_label.TabIndex = 5;
            this.Content_label.Text = "Content";
            // 
            // reload
            // 
            this.reload.ForeColor = System.Drawing.SystemColors.ControlText;
            this.reload.Location = new System.Drawing.Point(118, 6);
            this.reload.Name = "reload";
            this.reload.Size = new System.Drawing.Size(56, 63);
            this.reload.TabIndex = 6;
            this.reload.TileImage = ((System.Drawing.Image)(resources.GetObject("reload.TileImage")));
            this.reload.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reload.UseTileImage = true;
            this.reload.Click += new System.EventHandler(this.reload_Click);
            // 
            // Content_label_2
            // 
            this.Content_label_2.AutoSize = true;
            this.Content_label_2.Location = new System.Drawing.Point(489, 329);
            this.Content_label_2.Name = "Content_label_2";
            this.Content_label_2.Size = new System.Drawing.Size(55, 19);
            this.Content_label_2.TabIndex = 10;
            this.Content_label_2.Text = "Content";
            // 
            // Subject_label_2
            // 
            this.Subject_label_2.AutoSize = true;
            this.Subject_label_2.Location = new System.Drawing.Point(489, 262);
            this.Subject_label_2.Name = "Subject_label_2";
            this.Subject_label_2.Size = new System.Drawing.Size(51, 19);
            this.Subject_label_2.TabIndex = 9;
            this.Subject_label_2.Text = "Subject";
            // 
            // Subject_2_txt
            // 
            this.Subject_2_txt.Location = new System.Drawing.Point(489, 284);
            this.Subject_2_txt.Multiline = true;
            this.Subject_2_txt.Name = "Subject_2_txt";
            this.Subject_2_txt.ReadOnly = true;
            this.Subject_2_txt.Size = new System.Drawing.Size(245, 27);
            this.Subject_2_txt.TabIndex = 8;
            // 
            // Post_2_txt
            // 
            this.Post_2_txt.Location = new System.Drawing.Point(489, 351);
            this.Post_2_txt.Multiline = true;
            this.Post_2_txt.Name = "Post_2_txt";
            this.Post_2_txt.ReadOnly = true;
            this.Post_2_txt.Size = new System.Drawing.Size(245, 159);
            this.Post_2_txt.TabIndex = 7;
            // 
            // Content_label_3
            // 
            this.Content_label_3.AutoSize = true;
            this.Content_label_3.Location = new System.Drawing.Point(169, 541);
            this.Content_label_3.Name = "Content_label_3";
            this.Content_label_3.Size = new System.Drawing.Size(55, 19);
            this.Content_label_3.TabIndex = 14;
            this.Content_label_3.Text = "Content";
            // 
            // Subject_label_3
            // 
            this.Subject_label_3.AutoSize = true;
            this.Subject_label_3.Location = new System.Drawing.Point(169, 474);
            this.Subject_label_3.Name = "Subject_label_3";
            this.Subject_label_3.Size = new System.Drawing.Size(51, 19);
            this.Subject_label_3.TabIndex = 13;
            this.Subject_label_3.Text = "Subject";
            // 
            // Subject_3_txt
            // 
            this.Subject_3_txt.Location = new System.Drawing.Point(169, 496);
            this.Subject_3_txt.Multiline = true;
            this.Subject_3_txt.Name = "Subject_3_txt";
            this.Subject_3_txt.ReadOnly = true;
            this.Subject_3_txt.Size = new System.Drawing.Size(245, 27);
            this.Subject_3_txt.TabIndex = 12;
            // 
            // Post_3_txt
            // 
            this.Post_3_txt.Location = new System.Drawing.Point(169, 563);
            this.Post_3_txt.Multiline = true;
            this.Post_3_txt.Name = "Post_3_txt";
            this.Post_3_txt.ReadOnly = true;
            this.Post_3_txt.Size = new System.Drawing.Size(245, 159);
            this.Post_3_txt.TabIndex = 11;
            // 
            // Date_newest_txt
            // 
            this.Date_newest_txt.Location = new System.Drawing.Point(239, 192);
            this.Date_newest_txt.Name = "Date_newest_txt";
            this.Date_newest_txt.ReadOnly = true;
            this.Date_newest_txt.Size = new System.Drawing.Size(113, 23);
            this.Date_newest_txt.TabIndex = 15;
            // 
            // Date_2_txt
            // 
            this.Date_2_txt.Location = new System.Drawing.Point(621, 329);
            this.Date_2_txt.Name = "Date_2_txt";
            this.Date_2_txt.ReadOnly = true;
            this.Date_2_txt.Size = new System.Drawing.Size(113, 23);
            this.Date_2_txt.TabIndex = 16;
            // 
            // Date_3_txt
            // 
            this.Date_3_txt.Location = new System.Drawing.Point(301, 541);
            this.Date_3_txt.Name = "Date_3_txt";
            this.Date_3_txt.ReadOnly = true;
            this.Date_3_txt.Size = new System.Drawing.Size(113, 23);
            this.Date_3_txt.TabIndex = 17;
            // 
            // accounts
            // 
            this.accounts.ForeColor = System.Drawing.SystemColors.ControlText;
            this.accounts.Location = new System.Drawing.Point(826, 173);
            this.accounts.Name = "accounts";
            this.accounts.Size = new System.Drawing.Size(56, 63);
            this.accounts.TabIndex = 18;
            this.accounts.TileImage = ((System.Drawing.Image)(resources.GetObject("accounts.TileImage")));
            this.accounts.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.accounts.UseTileImage = true;
            this.accounts.Click += new System.EventHandler(this.accounts_Click);
            // 
            // Tawerna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 812);
            this.Controls.Add(this.accounts);
            this.Controls.Add(this.Date_3_txt);
            this.Controls.Add(this.Date_2_txt);
            this.Controls.Add(this.Date_newest_txt);
            this.Controls.Add(this.Content_label_3);
            this.Controls.Add(this.Subject_label_3);
            this.Controls.Add(this.Subject_3_txt);
            this.Controls.Add(this.Post_3_txt);
            this.Controls.Add(this.Content_label_2);
            this.Controls.Add(this.Subject_label_2);
            this.Controls.Add(this.Subject_2_txt);
            this.Controls.Add(this.Post_2_txt);
            this.Controls.Add(this.reload);
            this.Controls.Add(this.Content_label);
            this.Controls.Add(this.Subject_label);
            this.Controls.Add(this.Subject_newest_txt);
            this.Controls.Add(this.Post_newest_txt);
            this.Controls.Add(this.add);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Tawerna";
            this.Padding = new System.Windows.Forms.Padding(15, 60, 15, 16);
            this.Text = "Tawerna";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTile add;
        private MetroFramework.Controls.MetroTextBox Post_newest_txt;
        private MetroFramework.Controls.MetroTextBox Subject_newest_txt;
        private MetroFramework.Controls.MetroLabel Subject_label;
        private MetroFramework.Controls.MetroLabel Content_label;
        private MetroFramework.Controls.MetroTile reload;
        private MetroFramework.Controls.MetroLabel Content_label_2;
        private MetroFramework.Controls.MetroLabel Subject_label_2;
        private MetroFramework.Controls.MetroTextBox Subject_2_txt;
        private MetroFramework.Controls.MetroTextBox Post_2_txt;
        private MetroFramework.Controls.MetroLabel Content_label_3;
        private MetroFramework.Controls.MetroLabel Subject_label_3;
        private MetroFramework.Controls.MetroTextBox Subject_3_txt;
        private MetroFramework.Controls.MetroTextBox Post_3_txt;
        private MetroFramework.Controls.MetroTextBox Date_newest_txt;
        private MetroFramework.Controls.MetroTextBox Date_2_txt;
        private MetroFramework.Controls.MetroTextBox Date_3_txt;
        private MetroFramework.Controls.MetroTile accounts;
    }
}

