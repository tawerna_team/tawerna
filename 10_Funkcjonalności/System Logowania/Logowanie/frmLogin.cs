﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;


namespace Logowanie
{
    public partial class frmLogin : MetroFramework.Forms.MetroForm
    {
        static string connect;
        MySqlConnection myConnection;
        MySqlCommand cmd;
        MySqlDataReader reader;

        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            connect = "SERVER=localhost; DATABASE=dbuser; UID=root; PASSWORD=root;";
            myConnection = new MySqlConnection(connect);

            bool pass = false;
            string user = txtUser.Text;
            string password = txtPassword.Text;

            string query;

            if (string.IsNullOrEmpty(user))
            {
                MetroFramework.MetroMessageBox.Show(this, "Podaj swoją nazwę użytkownika.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUser.Focus();
                return;
            }

            if (string.IsNullOrEmpty(password))
            {
                MetroFramework.MetroMessageBox.Show(this, "Podaj swoje hasło.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPassword.Focus();
                return;
            }

            try
            {
                if (myConnection.State == ConnectionState.Closed)
                    myConnection.Open();

                query = "SELECT * FROM users";
                cmd = new MySqlCommand(query, myConnection); // wykonanie zapytania
                reader = cmd.ExecuteReader(); // czytanie zmiennych z bazy danych

                while (reader.Read())
                {
                    if (reader["usr_id"] != null && reader["usr_name"] != null && reader["usr_password"] != null)
                    {
                        if (reader["usr_name"].ToString() == user && reader["usr_password"].ToString() == password)
                        {
                            pass = true;
                            break;
                        }
                        else
                            pass = false;
                    }
                    else
                        pass = false;
                }
                reader.Close();

                if (pass)
                {
                    using (frmMain frm = new frmMain())
                    {
                        this.Hide();
                        frm.ShowDialog();
                    }
                }
                else
                    MetroFramework.MetroMessageBox.Show(this, "Błędna nazwa użytkownika lub hasło.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);            
            }
            catch (Exception ex)
            {
                MetroFramework.MetroMessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                myConnection.Close();
            }
        }
    }
}
