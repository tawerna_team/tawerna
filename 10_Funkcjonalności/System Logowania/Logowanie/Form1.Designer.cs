﻿namespace Logowanie
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mtLogin = new MetroFramework.Controls.MetroTile();
            this.mtRegistration = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // mtLogin
            // 
            this.mtLogin.ActiveControl = null;
            this.mtLogin.Location = new System.Drawing.Point(133, 60);
            this.mtLogin.Name = "mtLogin";
            this.mtLogin.Size = new System.Drawing.Size(87, 68);
            this.mtLogin.TabIndex = 1;
            this.mtLogin.Text = "Logowanie";
            this.mtLogin.UseSelectable = true;
            this.mtLogin.Click += new System.EventHandler(this.mtLogin_Click);
            // 
            // mtRegistration
            // 
            this.mtRegistration.ActiveControl = null;
            this.mtRegistration.Location = new System.Drawing.Point(23, 60);
            this.mtRegistration.Name = "mtRegistration";
            this.mtRegistration.Size = new System.Drawing.Size(87, 68);
            this.mtRegistration.TabIndex = 2;
            this.mtRegistration.Text = "Rejestracja";
            this.mtRegistration.UseSelectable = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(243, 155);
            this.Controls.Add(this.mtRegistration);
            this.Controls.Add(this.mtLogin);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Resizable = false;
            this.Text = "Tawerna";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile mtLogin;
        private MetroFramework.Controls.MetroTile mtRegistration;

    }
}

